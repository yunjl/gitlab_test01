package com.yun.service;

import com.yun.pojo.Books;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author Yunjl
 * @create 2021-06-13 8:12
 * 业务层
 */
public interface BookService {
    //添加一本书
    int addBook(Books books);

    //删除一本书
    int deleteBookById(int id);

    //更新一本书
    int updateBook(Books books);

    //查询一本书
    Books queryBookById(int id);

    //根据书名查询书籍
    Books  queryBookByName(@Param("bookName") String name);

    //查询全部的书
    List<Books> queryAllBook();

}
