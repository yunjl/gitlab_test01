package com.yun.service.Impl;

import com.yun.dao.BookMapper;
import com.yun.pojo.Books;
import com.yun.service.BookService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Yunjl
 * @create 2021-06-13 8:14
 * 业务层（service）调用dao层
 */
public class BookServiceImpl implements BookService {

//    注入dao层
    private BookMapper bookMapper;

    public void setBookMapper(BookMapper bookMapper) {
        this.bookMapper = bookMapper;
    }


    public int addBook(Books books) {
        return bookMapper.addBook(books);
    }

    public int deleteBookById(int id) {
        return bookMapper.deleteBookById(id);
    }

    public int updateBook(Books books) {
        return bookMapper.updateBook(books);
    }

    public Books queryBookById(int id) {
        return bookMapper.queryBookById(id);
    }

    public Books queryBookByName(String name) {
        return bookMapper.queryBookByName(name);
    }

    public List<Books> queryAllBook() {
        return bookMapper.queryAllBook();
    }
}
