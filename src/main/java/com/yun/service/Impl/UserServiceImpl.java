package com.yun.service.Impl;

import com.yun.dao.UserMapper;
import com.yun.pojo.User;
import com.yun.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Yunjl
 * @create 2021-06-13 21:37
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserMapper userMapper;

    public void setUserMapper(UserMapper userMapper) {
        this.userMapper = userMapper;
    }

    public User loginUser(User user) {
        return userMapper.loginUser(user);
    }

    public int createUser(User user) {
        return userMapper.createUser(user);
    }

    public User queryUserById(String userId) {
        return userMapper.queryUserById(userId);
    }

    public int correctUser(User user) {
        return userMapper.correctUser(user);
    }

    public void uploadImg(String userId, String userImg) {
        userMapper.uploadImg(userId,userImg);
    }

}
