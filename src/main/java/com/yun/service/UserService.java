package com.yun.service;

import com.yun.pojo.User;

/**
 * @author Yunjl
 * @create 2021-06-13 21:37
 */
public interface UserService {
    /**
     * 用户登录
     * @param user
     */
    User loginUser(User user);

    /**
     * 注册
     * @param user 所注册的用户
     */
    int createUser(User user);

    /**
     * 根据用户名查询
     * @param userId 用户id
     */
    User queryUserById(String userId);

    /**
     * 修改个人信息
     * @param user 所修改的用户
     */
    int correctUser(User user);

    /**
     *  根据用户id 修改头像
     * @param userId
     * @param userImg
     */
    void uploadImg(String userId, String userImg);
}
