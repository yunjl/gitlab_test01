package com.yun.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @author Yunjl
 * @create 2021-06-13 21:28
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class User {

    private String userId;
    private String userPassword;
    private String userImg;
    private String userEmail;
    private String userMassage;

}