package com.yun.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @author Yunjl
 * @create 2021-06-13 0:03
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Books {
    private Integer bookID;
    private String bookName;
    private Integer bookCounts;
    private String detail;

}
