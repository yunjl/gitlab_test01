package com.yun.dao;

import com.yun.pojo.User;
import org.apache.ibatis.annotations.Param;

/**
 * @author Yunjl
 * @create 2021-06-13 21:29
 */
public interface UserMapper {
    /**
     * 用户登陆
     * @param user
     * @return
     */
    User loginUser(User user);

    /**
     * 注册用户
     * @param user
     * @return
     */

    int createUser(User user);

    /**
     * 查询用户 通过id
     * @param userId
     * @return  返回查询出的用户
     */
    User queryUserById(@Param("userId")String userId);

    /**
     * 修改用户信息
     * @param user
     * @return  返回修改后的
     */
    int correctUser(User user);

    /**
     * 根据id修改头像
     * @param userId
     * @param userImg
     * @return
     */
    void uploadImg(@Param("userId")String userId, @Param("userImg")String userImg);
}
