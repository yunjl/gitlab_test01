package com.yun.controller;

import com.yun.pojo.User;
import com.yun.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
/**
 * @author Yunjl
 * @create 2021-06-13 22:54
 */
@Controller
@RequestMapping("/user")
public class UserController {

    @Autowired
    @Qualifier("UserServiceImpl")
    private UserService userService;

    HttpServletRequest request1;
    /**
     * 去登陆
     */
    @RequestMapping("/toLogin")
    public String tologinUser(){
        return "login";

    }

    /**
     * 登陆
     */
    @RequestMapping("/login")
    public String loginUser(User user, Model model, HttpServletRequest request){

        System.out.println("获取到"+user);
        User user1= userService.loginUser(user);
        System.out.println("查询到"+user1);
        if(user1!=null){
            //登陆成功
            model.addAttribute("UserID",user1.getUserId());

            request1=request;
            HttpSession session = request.getSession();
            session.setAttribute("user",user1);

            return "redirect:/book/allBook";
        }else {
            //登陆失败
            String s="用户名或密码错误";
            model.addAttribute("Error",s);
            return "login";
        }

    }
    /**
     * 注册
     */
    @RequestMapping("/toRegister")
    public String toRegisterUser(){
        return "register";

    }
    /**
     * 注册
     */
    @RequestMapping("/register")
    public String registerUser(User user, Model model){

        System.out.println("获取到"+user);
        User user1= userService.queryUserById(user.getUserId());
        System.out.println("查询到"+user1);
        if(user1!=null){
            //该id用户已经存在
            String s="用户名重复";
            model.addAttribute("registerError",s);
            return "register";
        }else {
            //可以注册
            userService.createUser(user);
            return "login";
        }

    }

    /**
     * 查看个人信息
     */
    @RequestMapping("/toCorrect")
    public String showUser( Model model){
        HttpSession session = request1.getSession();
        User user = (User)session.getAttribute("user");

        model.addAttribute("myUser",user);
        return "correctMsg";
    }

    @RequestMapping("/showMsg")
    public String showMsg( Model model){
        HttpSession session = request1.getSession();

        User user = (User)session.getAttribute("user");

        model.addAttribute("myUser",user);
        return "showMsg";

    }

    /**
     * 修改个人信息
     */
    @RequestMapping("/correctUser")
    public String correctUser(User user){
        userService.correctUser(user);

        return "redirect:/book/allBook";
    }

//    //访问秘钥
//    private static final String accessKey = "ypwMGL8-vy-BlWWphdyjRqTa4_ME3ePW-B0HpCwV";
//    //授权秘钥
//    private static final String secretKey = "uzSgVxWG-mq_3raG3oinlGhwbKMBZ4HDX2lOywZR";
//    //存储空间名称
//    private static final String bucket = "book-managent";
//    //外链域名
//    private static final String domain = "http://qnh3puqub.hn-bkt.clouddn.com/";
//
//    /**
//     * 上传个人头相
//     */
//    @RequestMapping("/toUploadImg")
//    public String uploadUserImg(){
//
//        return "uploadImg";
//    }
//
//    /**
//     * 上传个人头像
//     * @param userImg   图片
//     * @param  用户名
//     * @return
//     */
//    @RequestMapping("/uploadImg")
//    public String showUserImg(@RequestParam MultipartFile userImg){
//
//        HttpSession session = request1.getSession();
//        User user = (User)session.getAttribute("user");
//
//        if (userImg.isEmpty()) {
//
//            System.out.println("文件为空，请重新上传");
//        }
//
//        try {
//            byte[] bytes = userImg.getBytes();
//            String imageName = UUID.randomUUID().toString();
//            try {
//                //使用base64方式上传到七牛云
//                String userImgUrl = QiniuCloudUtil.put64image(bytes, imageName);
//
//                System.out.println("文件上传成功");
//
//                System.out.println(userImgUrl);
//                userService.uploadImg(user.getUserId(), userImgUrl);
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        } catch (IOException e) {
//
//            System.out.println("文件上传发生异常！");
//        }
//
//        return "redirect:/book/allBook";
//    }
    //    /**
//     * 上传个人头像
//     * @param userImg   图片
//     * @param userId  用户名
//     * @return
//     */
//    @RequestMapping("/uploadImg")
//    public String showUserImg(@RequestParam MultipartFile userImg,
//                              @RequestParam String userId){
//
//
//        if (userImg.isEmpty()) {
//
//            System.out.println("文件为空，请重新上传");
//        }
//
//        try {
//
//
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        } catch (IOException e) {
//
//            System.out.println("文件上传发生异常！");
//        }
//
//        return "redirect:/book/allBook";
//    }
//
    /**
     * 修改  个人头像(其实与上传一致)
     */
    @RequestMapping("/correctUserImg")
    public String correctUserImg(){

        return null;
    }
}
