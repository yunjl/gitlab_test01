package com.yun.controller;

import com.yun.pojo.Books;
import com.yun.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Yunjl
 * @create 2021-06-13 9:17
 * controller层调用service层
 */
@Controller                     //创建对象
@RequestMapping("/book")        //请求映射
public class BookController {

//    注入service层对象
    @Autowired                      //按类型装配对象
    @Qualifier("BookServiceImpl")   //该注解是用来消除依赖注入冲突的，指出我们想要使用哪个对象
    private BookService bookService;

    //查询全部书籍，并且返回一个书籍展示页面
    @RequestMapping("/allBook")
    public String list(Model model){
        List<Books> books = bookService.queryAllBook();

        model.addAttribute("list",books);
        return "allBook";   //返回allBook.jsp页面
    }

    //跳转到增加书籍
    @RequestMapping("/toAddBook")
    public String toAddPaper(){
        return "addBook";
    }

    //添加书籍的请求
    @RequestMapping("/addBook")
    public String addBook(Books books){
        System.out.println("添加书籍： " + books.toString());
        bookService.addBook(books);

        return "redirect:/book/allBook"; //重定向到allBook请求（实现了请求复用）/也可以直接重定向到allBook.jsp
    }

    //跳转到修改页面
    @RequestMapping("/toUpdateBook")
    public String toUpdatePaper(int id,Model model){
        Books books = bookService.queryBookById(id);
        model.addAttribute("Qbook",books);    //需要修改的book对象通过Qbook传入model
        return "updateBook";
    }

    //修改数据
    @RequestMapping("/updateBook")
    public String updateBook(Books books){

        System.out.println("修改的书籍：" + books);
        bookService.updateBook(books);
        return "redirect:/book/allBook";//重定向到allBook请求,会更新数据
    }

    //删除书籍
    @RequestMapping("/deleteBook/{bookId}") //前端传来数据并赋值给bookId      /deleteBook/${book.getBookID()}
    public String deleteBook(@PathVariable("bookId") int id){   //获取bookId的值并赋值给id
        bookService.deleteBookById(id);
        return "redirect:/book/allBook";
    }

    //根据书名查询书籍
    @RequestMapping("/queryBookByName")
    public String queryBookByName(String bookName,Model model){
        Books books = bookService.queryBookByName(bookName);
        System.out.println("查询的书籍："+bookName);

        List<Books> list =new ArrayList<Books>();
        list.add(books);

        if(books == null){
            model.addAttribute("error","未查到");
             list = bookService.queryAllBook();
        }
        model.addAttribute("list",list);
        return "allBook";
    }

}





