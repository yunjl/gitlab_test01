<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String basePath = request.getScheme() + "://" +
            request.getServerName() + ":" + request.getServerPort() +
            request.getContextPath() + "/";
%>
<html>
<head>
    <title>信息修改</title>
    <base href="<%=basePath%>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- 引入 Bootstrap -->
    <link href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
    <div class="container">

        <div class="row clearfix">
            <div class="col-md-12 column">
                <div class="page-header">
                    <h1>
                        <small>修改信息</small>
                    </h1>
                </div>
            </div>
        </div>

        <form action="${pageContext.request.contextPath}/book/updateBook" method="post">
            <%--这个id设置为隐藏域，为SQL指明需要修改的Book的id--%>
            <input type="hidden" name="bookID" value="${Qbook.getBookID()}"/>
            书籍名称：<input type="text" class="form-control" name="bookName" value="${Qbook.getBookName()}"/>
            书籍数量：<input type="text" class="form-control" name="bookCounts" value="${Qbook.getBookCounts()}"/>
            书籍详情：<input type="text" class="form-control" name="detail" value="${Qbook.getDetail() }"/>
            <input type="submit" class="btn btn-primary" value="提交"/>
        </form>

    </div>

</body>
</html>
