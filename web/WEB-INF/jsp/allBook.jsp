<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String basePath = request.getScheme() + "://" +
            request.getServerName() + ":" + request.getServerPort() +
            request.getContextPath() + "/";
%>
<html>
<head>

        <script type="text/javascript">
            window.onload = function (){
                //var trObj = $(this).parent().parent();
                var byId = document.getElementById("del");
                byId.onclick = function (){
                    //alert("js 原生的单击事件")
                    /**
                     * confirm 是JavaScript语言提供的一个确认提示框函数。你给它传什么，它就提示什么<br/>
                     * 当用户点击了确定，就返回true。当用户点击了取消，就返回false
                     */
                    if( confirm("你确定要删除吗？") ){
                        $trObj.remove();
                    }
                    // return false; 可以阻止a标签的默认行为(页面跳转)。
                    return false;
                }
            }
        </script>


    <title>数据展示页面</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- 引入 Bootstrap 美化界面-->
    <link href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">

</head>
<body>

<div class="container">

    <div class="row clearfix">
        <div class="col-md-12 column" >
            <div class="page-header">
                <h1>
                    <small>书籍列表 —— 显示所有书籍 </small>
                    <strong style="margin-right: 20%"> ${myUser.userId} 欢迎你</strong>
                </h1>
                <img src="${myUser.userImg}" height="100px" width="100px">
            </div>
        </div>
    </div>

    <div class="row" >
        <div class="col-md-8 column">
            <%--根据书名查询书籍--%>
            <form class="form-inline" action="${pageContext.request.contextPath}/book/queryBookByName" method="post" >
                <span style="color: red;font-weight: bold">${error}</span>
                <input type="text" class="form-control" name="bookName" placeholder="请输入所要查询的书名">
                <input type="submit" class="btn btn-primary" value="查询"/>
            </form>
            <a class="btn btn-primary" href="${pageContext.request.contextPath}/book/allBook">查询所有</a>
            <a class="btn btn-primary" href="${pageContext.request.contextPath}/book/toAddBook">新增书籍</a>
            <a class="btn btn-primary" href="${pageContext.request.contextPath}/user/showMsg">查看个人信息</a>

            <a class="btn btn-primary"
               href="${pageContext.request.contextPath}/user/toCorrect?userId=${requestScope.get("UserID")}">修改个人信息</a>

        </div>

    </div>

    <div class="row clearfix">
        <div class="col-md-12 column">
            <table class="table table-hover table-striped">
                <thead>
                <tr>
                    <th>书籍编号</th>
                    <th>书籍名字</th>
                    <th>书籍数量</th>
                    <th>书籍详情</th>
                    <th>操作</th>
                </tr>
                </thead>

                <tbody>
                <%--书籍数据从数据库通过model插入list--%>
                <c:forEach var="book" items="${list}">
                    <tr>
                        <td>${book.getBookID()}</td>
                        <td>${book.getBookName()}</td>
                        <td>${book.getBookCounts()}</td>
                        <td>${book.getDetail()}</td>
                        <td>

                            <a href="${pageContext.request.contextPath}/book/toUpdateBook?id=${book.getBookID()}">修改</a> |
                            <a  id="del" href="${pageContext.request.contextPath}/book/deleteBook/${book.getBookID()}">删除</a>
                        </td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
</div>
</body>
</html>