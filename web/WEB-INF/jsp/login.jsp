<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String basePath = request.getScheme() + "://" +
            request.getServerName() + ":" + request.getServerPort() +
            request.getContextPath() + "/";
%>
<html>
<head>
    <title>用户登录</title>
    <base href="<%=basePath%>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- 引入 Bootstrap -->
    <link href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <script type="text/javascript" >
        /*页面加载完成*/
        $(function () {
            $("#loginBtn").click(function () {
                //点击后在获取值 不然会出现获取值与逻辑顺序不符
                //获取用户名和密码
                var name=$("#name").val();
                var pwd=$("#pwd").val();
                // alert("判断");
                //判空
                if(isempty(name)){
                    $("#msg").html("用户名不能为空");
                    return;
                }
                if(isempty(pwd)){
                    $("#msg").html("密码不能为空");
                    return;
                }
                $("#loginForm").submit();
            });
        });
        //function 放在外面
        function isempty(str) {
            if(str==null||str.trim() == ""){
                //  alert(str+"为空");
                return true;
            }
            //alert("判空失败");
            return false;
        }
    </script>
</head>
<body>
    <div style="text-align: center;width:50%;margin-left:20%;margin-top: 20%" >
        <form  action="${pageContext.request.contextPath}/user/login" method="post" id="loginForm">
            <div class="input-group input-group-lg">
                <span class="input-group-addon">用户名：</span>
                <input type="text" class="form-control" name="userId" id="name"
                       placeholder="UserId" aria-describedby="sizing-addon1" required>
            </div>
            <div class="input-group input-group-lg">
                <span class="input-group-addon" >密  码：</span>
                <input type="password" class="form-control" name="userPassword" id="pwd"
                       placeholder="UserPassword" aria-describedby="sizing-addon1" required>
            </div>
            <%--id="loginBtn"--%>
            <span  style="font-size: 12px;color: red">${requestScope.get("Error")}</span> <br>
            <button type="submit" class="btn btn-primary" >登陆</button>
            <a href="${pageContext.request.contextPath}/user/toRegister"
               class="btn btn-primary">注册</a>

        </form>
    </div>
</body>
</html>
