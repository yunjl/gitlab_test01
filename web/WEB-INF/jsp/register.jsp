<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>新用户注册</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- 引入 Bootstrap -->
    <link href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">

    <script type="text/javascript" >
        $(function () {

            // $("#loginBtn").onblur(function () {
            // });
        });

    </script>
</head>
<body>
<div style="text-align: center;width:50%;margin-left:20%;margin-top: 20%" >
    <form  action="${pageContext.request.contextPath}/user/register" method="post">
        <div class="input-group input-group-lg">
            <span class="input-group-addon">用户名：</span>
            <input type="text" class="form-control" name="userId"  id="userId"
                   placeholder="UserId" aria-describedby="sizing-addon1" required>
        </div>
        <div class="input-group input-group-lg">
            <span class="input-group-addon" >密 码：</span>
            <input type="password" class="form-control" name="userPassword"
                   placeholder="UserPassword" aria-describedby="sizing-addon1" required>
        </div>
        <div class="input-group input-group-lg">
            <span class="input-group-addon" >邮 箱：</span>
            <input type="password" class="form-control" name="userEmail"
                   placeholder="UserEmail" aria-describedby="sizing-addon1" required>
        </div>
        <div class="input-group input-group-lg">
            <span class="input-group-addon" >个性签名：</span>
            <input type="password" class="form-control" name="userMassage"
                   placeholder="UserMassage" aria-describedby="sizing-addon1">
        </div>

        <span  style="font-size: 12px;color: red">
            ${requestScope.get("registerError")}</span> <br>

        <button type="submit" class="btn btn-primary" >注册</button>
    </form>
</div>
</body>
</html>