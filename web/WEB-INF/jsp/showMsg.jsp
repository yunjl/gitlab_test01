<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>个人信息查看页面</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- 引入 Bootstrap -->
    <link href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">

</head>
<body>
<div class="container">

    <div class="row clearfix">
        <div class="col-md-12 column">
            <div class="page-header">
                <h1>
                    <small>个人信息</small>
                </h1>
            </div>
        </div>
    </div>

    <form action="${pageContext.request.contextPath}/book/allBook" method="post">
        <%--这个id设置为隐藏域--%>
        <input type="hidden" name="userId" value="${myUser.userId}"/>
        密码：<input type="text" class="form-control" name="userPassword" value="${myUser.userPassword}"/>
        邮箱：<input type="text" class="form-control" name="userEmail" value="${myUser.userEmail}"/>
        个性签名：<input type="text" class="form-control" name="userMassage" value="${myUser.userMassage}"/>
        <input type="submit" class="btn btn-primary" value="返回"/>
    </form>

</div>
</body>
</html>