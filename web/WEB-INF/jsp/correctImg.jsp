<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>修改头像</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- 引入 Bootstrap -->
    <link href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">

</head>
<body>
<div class="container">

    <div class="row clearfix">
        <div class="col-md-12 column">
            <div class="page-header">
                <h1>
                    <small>修改信息</small>
                </h1>
            </div>
        </div>
    </div>

    <form action="${pageContext.request.contextPath}/user/correctUserImg" method="post">
        <%--这个id设置为隐藏域--%>
        <input type="hidden" name="userId" value="${myUser.getUserId()}"/>
        头像<input type="mal" class="form-control" name="userImg"
                 value="${myUser.getUserImg()}"/>

        <input type="submit" class="btn btn-primary" value="提交"/>
    </form>

</div>

</body>
</html>